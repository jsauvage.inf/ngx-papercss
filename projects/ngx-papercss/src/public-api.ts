/*
 * Public API Surface of ngx-papercss
 */

export * from './paper/ngx-papercss.module';

export * from './paper/components/paper-example-component/paper-example.component';
