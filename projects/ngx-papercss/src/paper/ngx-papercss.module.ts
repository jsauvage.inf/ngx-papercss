import { NgModule } from '@angular/core';
import { PaperExampleComponent } from './components/paper-example-component/paper-example.component';


@NgModule({
  declarations: [PaperExampleComponent],
  imports: [],
  exports: [PaperExampleComponent]
})
export class NgxPapercssModule { }
