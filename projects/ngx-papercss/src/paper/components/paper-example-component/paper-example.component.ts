import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'paper-example',
  template: `
    <p>
      example works!
    </p>
  `,
  styles: [
  ]
})
export class PaperExampleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
