import { CommonModule } from '@angular/common';
import { moduleMetadata } from '@storybook/angular';
// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Meta, Story } from '@storybook/angular/types-6-0';
import { PaperExampleComponent } from './paper-example.component';

export default {
  title: 'PaperCss/ExampleComponent',
  component: PaperExampleComponent,
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [CommonModule],
    }),
  ],
} as Meta;

const Template: Story<PaperExampleComponent> = (args: PaperExampleComponent) => ({
  component: PaperExampleComponent,
  props: args,
});

export const paperExampleComponent = Template.bind({});
paperExampleComponent.args = {
};
